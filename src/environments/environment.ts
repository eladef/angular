// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD6-x_uVkYIRVSRrZZUVH2VFHA4ACNwB_c",
    authDomain: "todos7-63ddd.firebaseapp.com",
    databaseURL: "https://todos7-63ddd.firebaseio.com",
    projectId: "todos7-63ddd",
    storageBucket: "todos7-63ddd.appspot.com",
    messagingSenderId: "455837117972"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
