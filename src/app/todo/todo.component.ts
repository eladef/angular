import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {

  @Input() data:any; // הגדרת אינפוט עם תכונה דטה
  @Output() myButtonClicked= new EventEmitter<any>();
  text;
  key;
  showEditField = false;
  showTheButton=true;
  tempText;


  delete(){
    this.todoService.delete(this.key)
  }

  save(){
    this.todoService.update(this.key, this.text)
    this.showEditField=false;
  }

  showButton(){
    this.showTheButton=true;
  }

  hideButton(){
    this.showTheButton=false;
  }

  toSend(){
    console.log('event caught'); //כלי חשוב שצריך להשתמש בו בפונקציות מורכבות
    this.myButtonClicked.emit(this.text);//  emit פולט אל האב
  }
  constructor( private todoService:TodosService) { }

  ngOnInit() { // בעת יצירת אלמנט טודו חדש, הפונקציה הזו רצה
    this.text = this.data.text;
this.key = this.data.$key;
  }

  showEdit(){
    this.showEditField=true;
    this.tempText=this.text;
  }

  cancel(){
    this.showEditField=false;
    this.text=this.tempText;
  }

}
