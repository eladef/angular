import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import{AuthService} from '../auth.service'; 
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos = [];
  text;

  addTodo(){
    this.todosService.addTodo(this.text)
    this.text='';
  }
    text1;
    showText($event){
      this.text1= $event;
    }

  constructor(private db:AngularFireDatabase, private authService:AuthService, private todosService:TodosService) { } //.החיבור שלנו לדאטהבייס. בעצם יותר אובייקט שמחבר אותנו
  ngOnInit() {
   
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(//אוביקט מסוג אנגולר שקראנו לו די בי. לאובייקט הזה קיימת פונקציה שנקראת ליסט. הפונקציה מקבלת אנדפוינט בשם טודוס וברגע שהיא מקבלת היא יודעת לקרוא את כל האובייקטים שנמצאים בה. ישנה מילה שמורה סנפשוט ציינגס שהיא בעצם אובסרבבל מוכן לשימוש
        todos=>{//ככה מתחילים את הפונקציה בכתיב של ארו פאנקשיין
          this.todos=[];//המערך שלנו מלמעלה
          todos.forEach(
            todo=>{//מערך חדש על בסיס הפיירבייס
              let y= todo.payload.toJSON();//בגלל שהענפים מגיעים כסטרינג, אנחנו ממירים אותם לגייסון
              y['$key'] = todo.key;//שורה שלא חייבים. אנחנו רוצים שבנוסף לשדות שלנו-סטטוס וטקסט, אנחנו נכניס גם את קי וכרגע למערך יהיו שלושה שדות. כרגע וואי הוא מערך המכיל שלושה שדות שוות בהיררכיה. הקי בכחול אחרי הנקודה היא מילה שמורה ולא קשורה לקייז עד עכשיו
              this.todos.push(y);
            }
          )
        }
      )
    })
   

  }

}
