import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {

  todos = [];


  text1;
  showText($event){
    this.text1= $event;
  }


  user='jack'
  constructor(private db:AngularFireDatabase) { } //.החיבור שלנו לדאטהבייס. בעצם יותר אובייקט שמחבר אותנו

  changeuser(){
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(//אוביקט מסוג אנגולר שקראנו לו די בי. לאובייקט הזה קיימת פונקציה שנקראת ליסט. הפונקציה מקבלת אנדפוינט בשם טודוס וברגע שהיא מקבלת היא יודעת לקרוא את כל האובייקטים שנמצאים בה. ישנה מילה שמורה סנפשוט ציינגס שהיא בעצם אובסרבבל מוכן לשימוש
      todos=>{//ככה מתחילים את הפונקציה בכתיב של ארו פאנקשיין
        this.todos=[];//המערך שלנו מלמעלה
        todos.forEach(
          todo=>{//מערך חדש על בסיס הפיירבייס
            let y= todo.payload.toJSON();//בגלל שהענפים מגיעים כסטרינג, אנחנו ממירים אותם לגייסון
            y['$key'] = todo.key;//שורה שלא חייבים. אנחנו רוצים שבנוסף לשדות שלנו-סטטוס וטקסט, אנחנו נכניס גם את קי וכרגע למערך יהיו שלושה שדות. כרגע וואי הוא מערך המכיל שלושה שדות שוות בהיררכיה. הקי בכחול אחרי הנקודה היא מילה שמורה ולא קשורה לקייז עד עכשיו
            this.todos.push(y);
          }
        )
      }
    )
  }

  
  ngOnInit() {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(//אוביקט מסוג אנגולר שקראנו לו די בי. לאובייקט הזה קיימת פונקציה שנקראת ליסט. הפונקציה מקבלת אנדפוינט בשם טודוס וברגע שהיא מקבלת היא יודעת לקרוא את כל האובייקטים שנמצאים בה. ישנה מילה שמורה סנפשוט ציינגס שהיא בעצם אובסרבבל מוכן לשימוש
      todos=>{//ככה מתחילים את הפונקציה בכתיב של ארו פאנקשיין
        this.todos=[];//המערך שלנו מלמעלה
        todos.forEach(
          todo=>{//מערך חדש על בסיס הפיירבייס
            let y= todo.payload.toJSON();//בגלל שהענפים מגיעים כסטרינג, אנחנו ממירים אותם לגייסון
            y['$key'] = todo.key;//שורה שלא חייבים. אנחנו רוצים שבנוסף לשדות שלנו-סטטוס וטקסט, אנחנו נכניס גם את קי וכרגע למערך יהיו שלושה שדות. כרגע וואי הוא מערך המכיל שלושה שדות שוות בהיררכיה. הקי בכחול אחרי הנקודה היא מילה שמורה ולא קשורה לקייז עד עכשיו
            this.todos.push(y);
          }
        )
      }
    )
  }

}

